﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Diagnostics;

namespace Documento
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCrearDocumento_Click(object sender, EventArgs e)
        {
            Document doc = new Document(PageSize.LETTER);
            PdfWriter writer = PdfWriter.GetInstance(doc,
                             new FileStream(@"C:\Users\SENA\Documents\prueba.pdf", FileMode.Create));

            doc.AddTitle("Mi primer PDF");
            doc.AddCreator("Jonathan Delgado Lopez");

            doc.Open();
            doc.Add(new Paragraph("Mi Primer documento PDF"));
            doc.Add(Chunk.NEWLINE);
            iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

            PdfPTable tblPrueba = new PdfPTable(3);
            tblPrueba.WidthPercentage = 100;

            PdfPCell clNombre = new PdfPCell(new Phrase("Nombre", _standardFont));
            clNombre.BorderWidth = 0;
            clNombre.BorderWidthBottom = 0.75f;

            PdfPCell clApellido = new PdfPCell(new Phrase("Apellido", _standardFont));
            clApellido.BorderWidth = 0;
            clApellido.BorderWidthBottom = 0.75f;

            PdfPCell clPais = new PdfPCell(new Phrase("Pais", _standardFont));
            clPais.BorderWidth = 0;
            clPais.BorderWidthBottom = 0.75f;

            tblPrueba.AddCell(clNombre);
            tblPrueba.AddCell(clApellido);
            tblPrueba.AddCell(clPais);

            PdfPCell cllNombre = new PdfPCell(new Phrase("Jonathan", _standardFont));
            cllNombre.BorderWidth = 0;
   

            PdfPCell cllApellido = new PdfPCell(new Phrase("Delgado", _standardFont));
            cllApellido.BorderWidth = 0;

            PdfPCell cllPais = new PdfPCell(new Phrase("Colombia", _standardFont));
            cllPais.BorderWidth = 0;


            tblPrueba.AddCell(cllNombre);
            tblPrueba.AddCell(cllApellido);
            tblPrueba.AddCell(cllPais);


            doc.Add(tblPrueba);
            doc.Close();
           writer.Close();


            MessageBox.Show("¡PDF Creado!");
        }
    }
}
